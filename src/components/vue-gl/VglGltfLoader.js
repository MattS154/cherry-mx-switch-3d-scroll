import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import VglLoader from '../../mixins/VglLoader';

const loader = new GLTFLoader();

export default {
  mixins: [VglLoader],
  computed: {
    loader() {
      return loader;
    }
  },
};
