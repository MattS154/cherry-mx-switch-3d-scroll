import VglObjLoader from "../vue-gl/VglObjLoader.js";
export default {
  mixins:[VglObjLoader],
  computed: {
    src() { return '/model/mx_parts/mx_pin.obj'; }
  },
};
